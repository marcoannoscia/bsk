package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;


public class FrameTest {

	@Test(expected = BowlingException.class)
	public void firstThrow11ShouldThrowsBowlingException() throws Exception{
		Frame frame = new Frame(11, 0);
	}
	
	@Test(expected = BowlingException.class)
	public void firstThrowNegativeShouldThrowsBowlingException() throws Exception{
		Frame frame = new Frame(-1, 0);
	}
	
	@Test(expected = BowlingException.class)
	public void secondThrowNegativeShouldThrowsBowlingException() throws Exception{
		Frame frame = new Frame(0, -1);
	}
	
	@Test(expected = BowlingException.class)
	public void secondThrow11ShouldThrowsBowlingException() throws Exception{
		Frame frame = new Frame(0, 11);
	}
	
	@Test
	public void testGetFirstThrow() throws Exception{
		Frame frame = new Frame(1, 2);
		assertEquals(1, frame.getFirstThrow()) ;
	}
	
	@Test
	public void testGetSecondThrow() throws Exception{
		Frame frame = new Frame(1, 2);
		assertEquals(2, frame.getSecondThrow()) ;
	}
	
	@Test
	public void testGetScore() throws Exception{
		Frame frame = new Frame(2, 6);
		assertEquals(8, frame.getScore()) ;
	}
	
	@Test
	public void testGetBonus() throws Exception{
		Frame frame = new Frame(2, 6);
		frame.setBonus(4);
		
		assertEquals(4, frame.getBonus()) ;
	}
	
	
	@Test
	public void frame91ShouldBeSpare() throws Exception{
		Frame frame = new Frame(9, 1);
		
		assertTrue(frame.isSpare()) ;
	}
	
	@Test
	public void frame41ShouldNotBeSpare() throws Exception{
		Frame frame = new Frame(4, 1);
		
		assertFalse(frame.isSpare()) ;
	}
	
	@Test
	public void testGetScoreOfFrameSpareWithBonus() throws Exception{
		Frame frame = new Frame(9, 1);
		frame.setBonus(3);
		
		assertEquals(13, frame.getScore()) ;
	}
	
	
	@Test
	public void testIsStrike() throws Exception{
		Frame frame = new Frame(10, 0);
		
		assertTrue(frame.isStrike()) ;
	}
	
	@Test
	public void Frame90ShouldbeNotStrike() throws Exception{
		Frame frame = new Frame(9, 0);
		
		assertFalse(frame.isStrike()) ;
	}
	

	
	@Test
	public void testGetScoreOfFrameStrikeWithBonus() throws Exception{
		Frame frame = new Frame(10, 0);
		frame.setBonus(5);
		
		assertEquals(15, frame.getScore()) ;
	}
}

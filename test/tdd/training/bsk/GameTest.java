package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {

	@Test
	public void testAddFrameToGame() throws Exception {
		Game game = new Game() ;
		Frame frame = new Frame(1,2) ;
		
		game.addFrame(frame);
		
		assertEquals(1, game.getCountFrames()) ;
	}
	
	
	@Test(expected = BowlingException.class)
	public void Adding11FrameShouldThrowsBowlingException() throws Exception {
		Game game = new Game() ;
		
		game.addFrame(new Frame(1,2));
		game.addFrame(new Frame(1,2));
		game.addFrame(new Frame(1,2));
		game.addFrame(new Frame(1,2));
		game.addFrame(new Frame(1,2));
		game.addFrame(new Frame(1,2));
		game.addFrame(new Frame(1,2));
		game.addFrame(new Frame(1,2));
		game.addFrame(new Frame(1,2));
		game.addFrame(new Frame(1,2));
		game.addFrame(new Frame(1,2));
		
	}
	
	
	@Test
	public void testGetFrameAt() throws Exception {
		Game game = new Game() ;
		Frame frame1 = new Frame(1,5) ;
		Frame frame2 = new Frame(3,6) ;
		Frame frame3 = new Frame(7,2) ;
		Frame frame4 = new Frame(3,6) ;
		Frame frame5 = new Frame(4,4) ;
		Frame frame6 = new Frame(5,3) ;
		Frame frame7 = new Frame(3,3) ;
		Frame frame8 = new Frame(4,5) ;
		Frame frame9 = new Frame(8,1) ;
		Frame frame10 = new Frame(2,6) ;
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		
		assertEquals(frame1, game.getFrameAt(0)) ;
		
	}
	
	
	@Test(expected = BowlingException.class)
	public void frameIndexOver10ShouldThrowsBowlingException() throws Exception {
		Game game = new Game() ;
		Frame frame1 = new Frame(1,5) ;
		Frame frame2 = new Frame(3,6) ;
		Frame frame3 = new Frame(7,2) ;
		Frame frame4 = new Frame(3,6) ;
		Frame frame5 = new Frame(4,4) ;
		Frame frame6 = new Frame(5,3) ;
		Frame frame7 = new Frame(3,3) ;
		Frame frame8 = new Frame(4,5) ;
		Frame frame9 = new Frame(8,1) ;
		Frame frame10 = new Frame(2,6) ;
		Frame frameCheck ;
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		
		frameCheck = game.getFrameAt(11) ;
		
	}
	
	
	@Test(expected = BowlingException.class)
	public void frameIndexUnder0ShouldThrowsBowlingException() throws Exception {
		Game game = new Game() ;
		Frame frame1 = new Frame(1,5) ;
		Frame frame2 = new Frame(3,6) ;
		Frame frame3 = new Frame(7,2) ;
		Frame frame4 = new Frame(3,6) ;
		Frame frame5 = new Frame(4,4) ;
		Frame frame6 = new Frame(5,3) ;
		Frame frame7 = new Frame(3,3) ;
		Frame frame8 = new Frame(4,5) ;
		Frame frame9 = new Frame(8,1) ;
		Frame frame10 = new Frame(2,6) ;
		Frame frameCheck ;
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		
		frameCheck = game.getFrameAt(-1) ;
		
	}
	
	
	
	@Test(expected = BowlingException.class)
	public void validIndexWithoutFrameShouldThrowsBowlingException() throws Exception {
		Game game = new Game() ;
		Frame frame1 = new Frame(1,5) ;
		Frame frame2 = new Frame(3,6) ;
		Frame frame3 = new Frame(7,2) ;
		Frame frame4 = new Frame(3,6) ;
		Frame frame5 = new Frame(4,4) ;
		Frame frameCheck ;
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);

		
		frameCheck = game.getFrameAt(6) ;
		
	}
	
	
	@Test
	public void testCalculateScore() throws Exception {
		Game game = new Game() ;
		Frame frame1 = new Frame(1,5) ;
		Frame frame2 = new Frame(3,6) ;
		Frame frame3 = new Frame(7,2) ;
		Frame frame4 = new Frame(3,6) ;
		Frame frame5 = new Frame(4,4) ;
		Frame frame6 = new Frame(5,3) ;
		Frame frame7 = new Frame(3,3) ;
		Frame frame8 = new Frame(4,5) ;
		Frame frame9 = new Frame(8,1) ;
		Frame frame10 = new Frame(2,6) ;
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		
		assertEquals(81, game.calculateScore()) ;
		
	}
	
	
	@Test(expected = BowlingException.class)
	public void calculateScoreOfNotCompletedGameShouldThrowsBowlingException() throws Exception {
		Game game = new Game() ;
		Frame frame1 = new Frame(1,5) ;
		Frame frame2 = new Frame(3,6) ;
		Frame frame3 = new Frame(7,2) ;
		Frame frame4 = new Frame(3,6) ;
		Frame frame5 = new Frame(4,4) ;
		int gameScore ;
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		
		
		gameScore = game.calculateScore() ;
		
	}
	
	
	
	@Test
	public void calculateScoreOfGameWithSpareFrame() throws Exception {
		Game game = new Game() ;
		Frame frame1 = new Frame(1,9) ;
		Frame frame2 = new Frame(3,6) ;
		Frame frame3 = new Frame(7,2) ;
		Frame frame4 = new Frame(3,6) ;
		Frame frame5 = new Frame(4,4) ;
		Frame frame6 = new Frame(5,3) ;
		Frame frame7 = new Frame(3,3) ;
		Frame frame8 = new Frame(4,5) ;
		Frame frame9 = new Frame(8,1) ;
		Frame frame10 = new Frame(2,6) ;
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		
		assertEquals(88, game.calculateScore()) ;
		
	}
	
	
	
	@Test
	public void calculateScoreOfGameWithStrikeFrame() throws BowlingException {
		Game game = new Game() ;
		Frame frame1 = new Frame(10,0) ;
		Frame frame2 = new Frame(3,6) ;
		Frame frame3 = new Frame(7,2) ;
		Frame frame4 = new Frame(3,6) ;
		Frame frame5 = new Frame(4,4) ;
		Frame frame6 = new Frame(5,3) ;
		Frame frame7 = new Frame(3,3) ;
		Frame frame8 = new Frame(4,5) ;
		Frame frame9 = new Frame(8,1) ;
		Frame frame10 = new Frame(2,6) ;
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		
		assertEquals(94, game.calculateScore()) ;
	}
	
	
	@Test
	public void calculateScoreOfGameWithStrikeFrameFollowedBySpareFrame() throws Exception {
		Game game = new Game() ;
		Frame frame1 = new Frame(10,0) ;
		Frame frame2 = new Frame(4,6) ;
		Frame frame3 = new Frame(7,2) ;
		Frame frame4 = new Frame(3,6) ;
		Frame frame5 = new Frame(4,4) ;
		Frame frame6 = new Frame(5,3) ;
		Frame frame7 = new Frame(3,3) ;
		Frame frame8 = new Frame(4,5) ;
		Frame frame9 = new Frame(8,1) ;
		Frame frame10 = new Frame(2,6) ;
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		
		assertEquals(103, game.calculateScore()) ;
	}
	
	
	@Test
	public void calculateScoreOfGameWithTwoConsecutiveStrikeFrame() throws Exception {
		Game game = new Game() ;
		Frame frame1 = new Frame(10,0) ;
		Frame frame2 = new Frame(10,0) ;
		Frame frame3 = new Frame(7,2) ;
		Frame frame4 = new Frame(3,6) ;
		Frame frame5 = new Frame(4,4) ;
		Frame frame6 = new Frame(5,3) ;
		Frame frame7 = new Frame(3,3) ;
		Frame frame8 = new Frame(4,5) ;
		Frame frame9 = new Frame(8,1) ;
		Frame frame10 = new Frame(2,6) ;
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		
		assertEquals(112, game.calculateScore()) ;
	}
	
	
	
	@Test
	public void calculateScoreOfGameWithTwoConsecutiveSpareFrame() throws Exception {
		Game game = new Game() ;
		Frame frame1 = new Frame(8,2) ;
		Frame frame2 = new Frame(5,5) ;
		Frame frame3 = new Frame(7,2) ;
		Frame frame4 = new Frame(3,6) ;
		Frame frame5 = new Frame(4,4) ;
		Frame frame6 = new Frame(5,3) ;
		Frame frame7 = new Frame(3,3) ;
		Frame frame8 = new Frame(4,5) ;
		Frame frame9 = new Frame(8,1) ;
		Frame frame10 = new Frame(2,6) ;
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		
		assertEquals(98, game.calculateScore()) ;
	}
	
	
	
	@Test
	public void testGetFirstBonusThrow() throws Exception {
		Game game = new Game() ;
		Frame frame1 = new Frame(1,5) ;
		Frame frame2 = new Frame(3,6) ;
		Frame frame3 = new Frame(7,2) ;
		Frame frame4 = new Frame(3,6) ;
		Frame frame5 = new Frame(4,4) ;
		Frame frame6 = new Frame(5,3) ;
		Frame frame7 = new Frame(3,3) ;
		Frame frame8 = new Frame(4,5) ;
		Frame frame9 = new Frame(8,1) ;
		Frame frame10 = new Frame(2,8) ;
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		game.setFirstBonusThrow(7);
		
		
		assertEquals(7, game.getFirstBonusThrow()) ;
	}
	
	
	@Test(expected = BowlingException.class)
	public void settingFirstBonusThrowWithNoLastFrameSpareShouldThrowBowlingException() throws Exception {
		Game game = new Game() ;
		Frame frame1 = new Frame(1,5) ;
		Frame frame2 = new Frame(3,6) ;
		Frame frame3 = new Frame(7,2) ;
		Frame frame4 = new Frame(3,6) ;
		Frame frame5 = new Frame(4,4) ;
		Frame frame6 = new Frame(5,3) ;
		Frame frame7 = new Frame(3,3) ;
		Frame frame8 = new Frame(4,5) ;
		Frame frame9 = new Frame(8,1) ;
		Frame frame10 = new Frame(1,8) ;
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		game.setFirstBonusThrow(7);
		
	}
	
	
	
	@Test
	public void testCalculateScoreWihtLastFrameSpare() throws Exception {
		Game game = new Game() ;
		Frame frame1 = new Frame(1,5) ;
		Frame frame2 = new Frame(3,6) ;
		Frame frame3 = new Frame(7,2) ;
		Frame frame4 = new Frame(3,6) ;
		Frame frame5 = new Frame(4,4) ;
		Frame frame6 = new Frame(5,3) ;
		Frame frame7 = new Frame(3,3) ;
		Frame frame8 = new Frame(4,5) ;
		Frame frame9 = new Frame(8,1) ;
		Frame frame10 = new Frame(2,8) ;
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		game.setFirstBonusThrow(7);
		
		
		assertEquals(90, game.calculateScore()) ;
	}
	
	
	@Test
	public void testGetSecondBonusThrow() throws Exception {
		Game game = new Game() ;
		Frame frame1 = new Frame(1,5) ;
		Frame frame2 = new Frame(3,6) ;
		Frame frame3 = new Frame(7,2) ;
		Frame frame4 = new Frame(3,6) ;
		Frame frame5 = new Frame(4,4) ;
		Frame frame6 = new Frame(5,3) ;
		Frame frame7 = new Frame(3,3) ;
		Frame frame8 = new Frame(4,5) ;
		Frame frame9 = new Frame(8,1) ;
		Frame frame10 = new Frame(10,0) ;
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		
		assertEquals(2, game.getSecondBonusThrow()) ;
	}
	
	@Test(expected = BowlingException.class)
	public void settingSecondBonusThrowWithNoLastFrameStrikeShouldThrowBowlingException() throws Exception {
		Game game = new Game() ;
		Frame frame1 = new Frame(1,5) ;
		Frame frame2 = new Frame(3,6) ;
		Frame frame3 = new Frame(7,2) ;
		Frame frame4 = new Frame(3,6) ;
		Frame frame5 = new Frame(4,4) ;
		Frame frame6 = new Frame(5,3) ;
		Frame frame7 = new Frame(3,3) ;
		Frame frame8 = new Frame(4,5) ;
		Frame frame9 = new Frame(8,1) ;
		Frame frame10 = new Frame(2,0) ;
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		
	}
	
	@Test(expected = BowlingException.class)
	public void settingSecondBonusThrowWithLastFrameSpareShouldThrowBowlingException() throws Exception {
		Game game = new Game() ;
		Frame frame1 = new Frame(1,5) ;
		Frame frame2 = new Frame(3,6) ;
		Frame frame3 = new Frame(7,2) ;
		Frame frame4 = new Frame(3,6) ;
		Frame frame5 = new Frame(4,4) ;
		Frame frame6 = new Frame(5,3) ;
		Frame frame7 = new Frame(3,3) ;
		Frame frame8 = new Frame(4,5) ;
		Frame frame9 = new Frame(8,1) ;
		Frame frame10 = new Frame(9,1) ;
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		
	}
	
	
	@Test
	public void testCalculateScoreWithLastFrameStrike() throws Exception {
		Game game = new Game() ;
		Frame frame1 = new Frame(1,5) ;
		Frame frame2 = new Frame(3,6) ;
		Frame frame3 = new Frame(7,2) ;
		Frame frame4 = new Frame(3,6) ;
		Frame frame5 = new Frame(4,4) ;
		Frame frame6 = new Frame(5,3) ;
		Frame frame7 = new Frame(3,3) ;
		Frame frame8 = new Frame(4,5) ;
		Frame frame9 = new Frame(8,1) ;
		Frame frame10 = new Frame(10,0) ;
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		assertEquals(92, game.calculateScore()) ;
		
	}
	
	
	@Test
	public void testCalculateScoreOfAPerfectGame() throws Exception {
		Game game = new Game() ;
		Frame frame1 = new Frame(10,0) ;
		Frame frame2 = new Frame(10,0) ;
		Frame frame3 = new Frame(10,0) ;
		Frame frame4 = new Frame(10,0) ;
		Frame frame5 = new Frame(10,0) ;
		Frame frame6 = new Frame(10,0) ;
		Frame frame7 = new Frame(10,0) ;
		Frame frame8 = new Frame(10,0) ;
		Frame frame9 = new Frame(10,0) ;
		Frame frame10 = new Frame(10,0) ;
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		
		assertEquals(300, game.calculateScore()) ;
		
	}

}

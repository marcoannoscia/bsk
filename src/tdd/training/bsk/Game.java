package tdd.training.bsk;

import java.util.ArrayList;

public class Game {
	
	private ArrayList<Frame> frames ;
	private int countFrames ;
	private int firstBonusThrow ;
	private int secondBonusThrow ;
	
	private static final int maxNumberOfFrames = 10 ;



	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		 frames = new ArrayList<Frame>() ;
		 countFrames = 0 ;
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if(countFrames == maxNumberOfFrames) {
			throw new BowlingException("It's not possible to add over 10 frames") ;
		}else {
			frames.add(frame) ;
			countFrames ++ ;
		}
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		if(checkIndex(index)) {
			throw new BowlingException("Index not valid") ; 
		}
		
		return frames.get(index) ;
	}

	private boolean checkIndex(int index) {
		return index > maxNumberOfFrames || index < 0 || index + 1 > countFrames ;
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		
		if(getLastFrame().isSpare() || getLastFrame().isStrike()) {
			this.firstBonusThrow = firstBonusThrow ;
		}else {
			throw new BowlingException("It's not possbile to set a bonus throw when the last frame is not a spare");
		}
	}

	private Frame getLastFrame() {
		return frames.get(maxNumberOfFrames-1);
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		if(getLastFrame().isStrike()) {
				this.secondBonusThrow = secondBonusThrow ;
		}else {
			throw new BowlingException("It's not possbile to set a second bonus throw when the last frame is not a strike") ;
		}
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrow ;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score = 0  ;
		
		if(!gameCompleted()) {
			throw new BowlingException("Game not completed yet") ;
		}
		
		for(Frame frame : frames) {
			if(frame.isSpare()) {
				calculateScoreOfSpareFrame(frame);
				
			}else if(frame.isStrike()) {
				calculateScoreOfStrikeFrame(frame);
			}
			
			score += frame.getScore() ;
		}
		
		return score ;
	}

	private void calculateScoreOfSpareFrame(Frame frame) {
		if(frame.equals(getLastFrame())) {
			frame.setBonus(firstBonusThrow);
		}else {
			frame.setBonus(getSubsequentFrameFirstThrow(frame));
		}
	}

	private void calculateScoreOfStrikeFrame(Frame frame) {
		if(frame.equals(getLastFrame())) {
			frame.setBonus(firstBonusThrow + secondBonusThrow);
		}else if(getSubsequentFrameOf(frame).isStrike()) {
			frame.setBonus(calculateConsecutiveStrikeFrameBonus(frame));
		}else {
			frame.setBonus(getSubsequentFrameFirstAndSecondThrows(frame));
		}
	}

	private int calculateConsecutiveStrikeFrameBonus(Frame frame) {
		if(getSubsequentFrameOf(frame).equals(getLastFrame()) && getLastFrame().isStrike()) {
			return getSubsequentFrameFirstThrow(frame) + firstBonusThrow ;
		}
		
		return getSubsequentFrameFirstThrow(frame) + 
				getSubsequentFrameFirstThrow(getSubsequentFrameOf(frame));
	}

	private Frame getSubsequentFrameOf(Frame frame) {
		return frames.get(frames.indexOf(frame)+1);
	}

	private int getSubsequentFrameFirstAndSecondThrows(Frame frame) {
		return getSubsequentFrameOf(frame).getFirstThrow() + 
				getSubsequentFrameOf(frame).getSecondThrow();
	}

	private int getSubsequentFrameFirstThrow(Frame frame) {
		return getSubsequentFrameOf(frame).getFirstThrow();
	}

	private boolean gameCompleted() {
		return countFrames == maxNumberOfFrames;
	}
	
	
	public int getCountFrames() {
		return countFrames ;
	}

}
